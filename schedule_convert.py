import os
import time
import json
import schedule
from docx2pdf import convert
import logging
# import threading
from pdp import pdp_rep_gen
from clifton import clifton_rep_gen
from disc_charts import ready_documents

# logging.basicConfig()
# schedule_logger = logging.getLogger('schedule')
# schedule_logger.setLevel(level=logging.DEBUG)

import sys
import requests
from pvq import pvq_gen_report

script = os.path.realpath(__file__)
path = os.path.dirname(script)
sys.path.extend([path])


def converter_pvg():
    script = os.path.realpath(__file__)
    path = os.path.dirname(script)
    sys.path.extend([path])
    file_path = os.path.join(path, "pvg_schedule.txt")
    f = open(file_path, "r", encoding='utf-8')
    lines = f.readlines()
    f.close()
    with open(file_path, "w", encoding='utf-8'):
        pass

    for line in lines:
        try:

            mydata = json.loads(line)
            file_name = os.path.join(os.path.join(path, "temp"), mydata["file_name"])
            pvq_gen_report(mydata["skills"], file_name, mydata["nid"], mydata.get("user", ""))
            convert(file_name)
            if mydata.get("dtoken"):
                requests.get("https://pdkm.ir/exam/result/?file_name=%s&token=%s" % (
                    mydata["file_name"][:-4] + "pdf", mydata["dtoken"]))
        except Exception as error:
            print(str(error))


def converter_pdp():
    script = os.path.realpath(__file__)
    path = os.path.dirname(script)
    sys.path.extend([path])
    file_path = os.path.join(path, "pdp_schedule.txt")
    f = open(file_path, "r", encoding='utf-8')
    lines = f.readlines()
    f.close()
    with open(file_path, "w", encoding='utf-8'):
        pass

    for line in lines:
        try:

            mydata = json.loads(line)
            file_name = os.path.join(os.path.join(path, "temp"), mydata["file_name"])
            pdp_rep_gen(mydata["skills"], file_name, mydata["pvq"], mydata.get("prior_job_idx"))
            convert(file_name)
            if mydata.get("dtoken"):
                requests.get("https://pdkm.ir/exam/result/?file_name=%s&token=%s" % (
                    mydata["file_name"][:-4] + "pdf", mydata["dtoken"]))


        except Exception as error:
            print(str(error))


def converter_clifton():
    script = os.path.realpath(__file__)
    path = os.path.dirname(script)
    sys.path.extend([path])
    file_path = os.path.join(path, "clifton_schedule.txt")
    f = open(file_path, "r", encoding='utf-8')
    lines = f.readlines()
    f.close()
    with open(file_path, "w", encoding='utf-8'):
        pass

    for line in lines:
        try:

            mydata = json.loads(line)
            file_name = os.path.join(os.path.join(path, "temp"), mydata["file_name"])
            clifton_rep_gen(mydata["skills"], file_name, mydata["nid"], mydata.get("user", ""))
            convert(file_name)
            if mydata.get("dtoken"):
                requests.get("https://pdkm.ir/exam/result/?file_name=%s&token=%s" % (
                    mydata["file_name"][:-4] + "pdf", mydata["dtoken"]))

        except Exception as error:
            print(str(error))


def converter_schedule():
    script = os.path.realpath(__file__)
    path = os.path.dirname(script)
    file_path = os.path.join(path, "convert_schedule.txt")
    f = open(file_path, "r", encoding='utf-8')
    lines = f.readlines()
    f.close()
    with open(file_path, "w", encoding='utf-8'):
        pass
    print(len(lines))
    for line in lines:
        try:
            mydata = json.loads(line)
            mydata["file_path"] = os.path.join(path, mydata["file_path"])
            print(mydata)
            file_name = os.path.join(os.path.join(path, "temp"), mydata["file_name"])
            print(file_name)
            ready_documents(full_file_name=file_name, **mydata)
            convert(file_name)
            if mydata.get("dtoken"):
                requests.get("https://pdkm.ir/exam/result/?file_name=%s&token=%s" % (
                    mydata["file_name"][:-4] + "pdf", mydata["dtoken"]))
        except Exception as error:
            print("khata kardam!")
            print(str(error))


if __name__ == '__main__':
    schedule.every(20).seconds.do(converter_schedule)
    schedule.every(20).seconds.do(converter_clifton)
    schedule.every(20).seconds.do(converter_pvg)
    schedule.every(20).seconds.do(converter_pdp)
    while True:
        schedule.run_pending()
        time.sleep(10)

