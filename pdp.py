from math import fabs, sqrt
import pandas as pd
import os, sys
from docx import Document
from clifton import SkillsInfo, char_decode
from docx.oxml import parse_xml
from docx.oxml.ns import nsdecls
from math import ceil
from clifton import clifton_rep_gen


def pdp_rep_gen(skill_str, full__file_name, pvq_dna, prior_job_idx):
    script = os.path.realpath(__file__)
    path = os.path.dirname(script)
    sys.path.extend([path])
    doc1 = Document(os.path.join(path, "PDP-Template2.docx"))
    fill_report(doc1, pvq_dna, skill_str, prior_job_idx)
    doc1.save(full__file_name)


def fill_pvq_part(pvq_dna, doc1):
    pvq_skill = ["خودرهبری", "هیجان‌خواهی", "لذت‌جویی", "موفقیت", "قدرت", "امنیت", "هم‌نوایی", "سنت‌گرایی", "خیرخواهی",
                 "جهان‌نگری "]
    doc1.tables[0].cell(0, 0).paragraphs[0].runs[0].text = pvq_skill[int(pvq_dna[0])]
    doc1.tables[0].cell(0, 1).paragraphs[0].runs[0].text = pvq_skill[int(pvq_dna[1])]
    doc1.tables[0].cell(0, 2).paragraphs[0].runs[0].text = pvq_skill[int(pvq_dna[2])]
    doc1.tables[1].cell(0, 0).paragraphs[0].runs[0].text = pvq_skill[int(pvq_dna[3])]
    doc1.tables[1].cell(0, 1).paragraphs[0].runs[0].text = pvq_skill[int(pvq_dna[4])]
    doc1.tables[1].cell(0, 2).paragraphs[0].runs[0].text = pvq_skill[int(pvq_dna[5])]
    doc1.tables[2].cell(0, 0).paragraphs[0].runs[0].text = pvq_skill[int(pvq_dna[6])]
    doc1.tables[2].cell(0, 1).paragraphs[0].runs[0].text = pvq_skill[int(pvq_dna[7])]
    doc1.tables[2].cell(0, 2).paragraphs[0].runs[0].text = pvq_skill[int(pvq_dna[8])]
    doc1.tables[3].cell(0, 0).paragraphs[0].runs[0].text = pvq_skill[int(pvq_dna[9])]


def fill_clifton_part(skill_str, doc1):
    hex_color_list = ["5d3a6d", "f7b32e", "204b75", "9c1c20"]
    for loop_index, ch in enumerate(skill_str):
        c_index = char_decode(ch)
        hex_color = hex_color_list[int(SkillsInfo[c_index]["color"]) - 1]
        shading_elm_1 = parse_xml((r'<w:shd {} w:fill="' + hex_color + r'"/>').format(nsdecls('w')))
        if loop_index < 10:
            doc1.tables[4].cell(2 * loop_index + 1, 8).paragraphs[0].runs[0].text = SkillsInfo[c_index]["name"]
            doc1.tables[4].cell(2 * loop_index + 1, 6)._tc.get_or_add_tcPr().append(shading_elm_1)

        elif loop_index < 22:
            doc1.tables[4].cell(2 * loop_index - 19, 5).paragraphs[0].runs[0].text = SkillsInfo[c_index]["name"]
            doc1.tables[4].cell(2 * loop_index - 19, 3)._tc.get_or_add_tcPr().append(shading_elm_1)
        else:
            doc1.tables[4].cell(2 * loop_index - 43, 2).paragraphs[0].runs[0].text = SkillsInfo[c_index]["name"]
            doc1.tables[4].cell(2 * loop_index - 43, 0)._tc.get_or_add_tcPr().append(shading_elm_1)


def fill_report(doc1, pvq_dna, skill_str, prior_job_index=None):
    fill_pvq_part(pvq_dna, doc1)
    fill_clifton_part(skill_str, doc1)
    pvq_cov = pd.read_excel("cov-groupsJob.xlsx", sheet_name="PVQ-Jobs")
    clifton_cov = pd.read_excel("cov-groupsJob.xlsx", sheet_name="Clifton-Jobs")
    pvq_lost = pvq_cov.iloc[:, pvq_cov.columns != "Unnamed: 0"].apply(
        lambda x: sqrt(sum(list(map(calc_pvq_loss_function, enumerate(x), [pvq_dna] * 10)))), axis=0).to_list()
    clifton_lost = clifton_cov.iloc[:, clifton_cov.columns != "Unnamed: 0"].apply(
        lambda x: sqrt(sum(list(map(calc_clifton_loss_function, enumerate(x), [skill_str] * 10)))), axis=0).to_list()
    result_lost = [sum(values) for values in zip(pvq_lost, clifton_lost)]
    df = pd.DataFrame(result_lost).sort_values(by=[0])

    sorted_job_index = list(df.index)
    sorted_job_name = [clifton_cov.columns[i + 1] for i in sorted_job_index]
    best_job_name = sorted_job_name[0]
    doc1.tables[6].cell(0, 1).text = best_job_name
    for i in range(0, 12):
        doc1.tables[5].cell(2 * i, 0).text = sorted_job_name[i]
    fill_skill_tables_in_order(best_job_name, doc1.tables[7], skill_str)
    if prior_job_index is None:
        delete_n_par(140, doc1)
        delete_n_par(137, doc1)
        doc1.tables[9]._element.getparent().remove(doc1.tables[8]._element)
        doc1.tables[8]._element.getparent().remove(doc1.tables[7]._element)
    else:
        prior_job = clifton_cov.columns[prior_job_index]
        doc1.tables[8].cell(0, 1).text = prior_job
        fill_skill_tables_in_order(prior_job, doc1.tables[9], skill_str)


def fill_skill_tables_in_order(job_name, table, skill_str):
    ordered_skills = calc_bordered_skill_clifton_user( skill_str,job_name)
    ordered_skills1 = ordered_skills[ordered_skills["type"] == 1]
    ordered_skills2 = ordered_skills[ordered_skills["type"] == 2]
    ordered_skills3 = ordered_skills[ordered_skills["type"] == 3]
    ordered_skills4 = ordered_skills[ordered_skills["type"] == 4]
    for i in range(0, 5):
        skill_name, sub_group, ratio = ordered_skills1.iloc[i]
        # table.cell(i + 2, 1).text = sub_group
        table.cell(i + 2, 1).text = skill_name
        # table.cell(i + 2, 4).text = str(round(ratio, 3))

    for i in range(0, 5):
        skill_name, sub_group, ratio = ordered_skills2.iloc[i]
        # table.cell(i + 8, 1).text = sub_group
        table.cell(i + 8, 1).text = skill_name
        # table.cell(i + 8, 4).text = str(round(ratio, 3))

    for i in range(0, 5):
        skill_name, sub_group, ratio = ordered_skills3.iloc[i]
        # table.cell(i + 39, 1).text = sub_group
        table.cell(i + 14, 1).text = skill_name
        # table.cell(i + 39, 4).text = str(round(ratio, 3))

    for i in range(0, 5):
        skill_name, sub_group, ratio = ordered_skills4.iloc[i]
        # table.cell(i + 92, 1).text = sub_group
        table.cell(i + 20, 1).text = skill_name
        # table.cell(i + 92, 4).text = str(round(ratio, 3))


def delete_n_par(i, doc):
    for j in range(i + 2, i - 2, -1):
        # delete the related 4 lines
        delete_paragraph(doc.paragraphs[j])


def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    paragraph._p = paragraph._element = None


def calc_pvq_loss_function(job_idx_and_item, pvq_dnd):
    job_idx, job_item = job_idx_and_item
    return pow(fabs(job_item - pvq_dnd.index(str(job_idx)) - 1), 2)


def clifton_char_encode(num):
    """
    :param num: from 0 to 33
    :return: char
    """
    if num <= 26:
        return chr(num + ord("A"))
    else:
        return chr(num + ord("a") - 27)


def calc_clifton_loss_function(job_idx_and_item, skill_str):
    job_idx, job_item = job_idx_and_item
    return pow(fabs(job_item - skill_str.index(clifton_char_encode(job_idx)) - 1), 2)


def clifton_str_decode(ch):
    num = ord(ch) - ord('A')
    if num < 26:
        return num
    else:
        return ord(ch) - ord("a") + 26


def calc_bordered_skill_clifton_user(skill_str, job_name):
    skill_tab1 = pd.read_excel("cov-groupsJob.xlsx", sheet_name="Skills-Jobs1")
    skill_tab2 = pd.read_excel("cov-groupsJob.xlsx", sheet_name="Skills-Jobs2")
    skill_tab3 = pd.read_excel("cov-groupsJob.xlsx", sheet_name="Skills-Jobs3")
    skill_tab4 = pd.read_excel("cov-groupsJob.xlsx", sheet_name="Skills-Jobs4")
    # table.cell(0, 3).text = job_name
    ordered_skills1 = skill_tab1[[skill_tab1.columns[0], skill_tab1.columns[1], job_name]]
    ordered_skills2 = skill_tab2[[skill_tab1.columns[0], skill_tab1.columns[1], job_name]]
    ordered_skills3 = skill_tab3[[skill_tab1.columns[0], skill_tab1.columns[1], job_name]]
    ordered_skills4 = skill_tab4[[skill_tab1.columns[0], skill_tab1.columns[1], job_name]]
    all_skills = ordered_skills1.append(ordered_skills2).append(ordered_skills3).append(ordered_skills4)

    skill_clifton = pd.read_excel("cov-groupsJob.xlsx", sheet_name="Skills-Clifton")
    standard_clifton_gap_border = [0] * 34
    for idx, ch in enumerate(skill_str):
        standard_clifton_gap_border[clifton_str_decode(ch)] = 1 - ((33 - idx) / 34)

    skill_clifton.iloc[:, 2:] = skill_clifton.iloc[:, 2:] * standard_clifton_gap_border
    skill_clifton['sum'] = skill_clifton.sum(axis=1)

    skill_clifton['sum'] = skill_clifton['sum'] * all_skills[job_name].to_list()
    skill_clifton["sum"] = skill_clifton["sum"] / (ceil(skill_clifton["sum"].max()) + 1)
    return skill_clifton[["نام دوره", "sum", "type"]].sort_values(['type', 'sum'],
                                                                  ascending=[True, False])


if __name__ == '__main__':
    pdp_str = {"file_name": "QB7IR.docx", "skills": "HOYTBXbEVKLZGIWfaCNSUegDQMJPhdRcFA", "pvq": "0123456789",
               "prior_job_idx": 10}

    pdp_rep_gen(pdp_str["skills"], pdp_str["file_name"], pdp_str["pvq"], pdp_str["prior_job_idx"])
