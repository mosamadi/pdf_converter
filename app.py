import os
import pythoncom
import json
from docx2pdf import convert
from flask import Flask, request, redirect, url_for, send_from_directory
import logging

from disc_charts import ready_documents, random_str_generator

from logging.handlers import RotatingFileHandler

from flask import Flask
import logging
import time
import os

app = Flask(__name__)

handler = RotatingFileHandler(os.path.join(app.root_path, 'logs', 'error_log.log'), maxBytes=102400, backupCount=10)
logging_format = logging.Formatter(
    '%(asctime)s - %(levelname)s - %(filename)s - %(funcName)s - %(lineno)s - %(message)s')

handler.setFormatter(logging_format)
app.logger.addHandler(handler)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    return response


@app.errorhandler(404)
def page_not_found(error):
    app.logger.error(error)

    return 'This page does not exist', 404


@app.errorhandler(500)
def special_exception_handler(error):
    app.logger.error(error)
    return '500 error', 500


app.error_handler_spec[None][404] = page_not_found


def en_num_to_farsi_num(input_text):
    input_text = str(input_text)
    intab = '12345678901234567890'
    outtab = '۱۲۳۴۵۶۷۸۹۰١٢٣٤٥٦٧٨٩٠'
    translation_table = str.maketrans(intab, outtab)
    return input_text.translate(translation_table)


@app.route("/", methods=["GET"])
def report_salam():
    return "salam!"


@app.route("/disc/report/<file_name>", methods=["GET"])
def report_satatus(file_name):
    directory_path = os.path.join(app.root_path, "temp")
    if os.path.exists(os.path.join(directory_path, file_name)):
        return {"status": "Completed"}
    else:
        return {"status": "In progress"}


@app.route("/disc/report", methods=["POST"])
def report_generator():
    req_arr = request.get_json(force=True)
    pythoncom.CoInitialize()
    personality = req_arr["personality"]
    main_dict = {
        "dtoken": req_arr.get("dtoken"),
        "file_path": f"disc_word_template\{personality}.docx",
                 "olgo_text": "الگوی رفتاری شما S با کد ۴-۳-۲-۱  می‌باشد."
                     .replace("۱", en_num_to_farsi_num(req_arr["d_seg"]))
                     .replace("۲", en_num_to_farsi_num(req_arr["i_seg"]))
                     .replace("۳", en_num_to_farsi_num(req_arr["s_seg"]))
                     .replace("۴", en_num_to_farsi_num(req_arr["c_seg"]))
                     .replace("S", personality)
                 }
    # mydict = {
    #
    #     "min_d": 44, "min_i": 22, "min_s": 11, "min_c": 17, "max_d": 74, "max_i": 22, "max_s": 34, "max_c": 44
    #     , "d_value": 55, "i_value": 66, "s_value": 95, "c_value": 44, "d_seg": 7, "i_seg": 5, "s_seg": 3, "c_seg": 2
    # }
    main_dict.update(req_arr)
    file_name = random_str_generator() + ".docx"
    main_dict["file_name"] = file_name

    with open("convert_schedule.txt", "a", encoding='utf-8') as f:
        json.dump(main_dict, f, ensure_ascii=False)
        f.write("\n")
    return file_name[-10:-5] + ".pdf"


@app.route("/clifton/report", methods=["POST"])
def clifton_report():
    req_arr = request.get_json(force=True)
    skill_str = req_arr["skill"]
    nid = req_arr["nid"]
    user = req_arr.get("user", "")
    file_name = random_str_generator() + ".docx"
    token = req_arr.get("dtoken")
    with open("clifton_schedule.txt", "a", encoding='utf-8') as f:
        json.dump({"file_name": file_name, "skills": skill_str, "nid": nid, "user": user,"dtoken":token}, f, ensure_ascii=False)
        f.write("\n")
    return file_name[-10:-5] + ".pdf"


@app.route("/pdp/report", methods=["POST"])
def pdp_report():
    req_arr = request.get_json(force=True)
    skill_str = req_arr["skills"]
    pvq_dna = req_arr["pvq"]
    prior_job_idx = req_arr.get("prior_job_idx", "")
    if prior_job_idx and str(prior_job_idx).isdigit():
        prior_job_idx = int(prior_job_idx)
    else:
        prior_job_idx = None
    file_name = random_str_generator() + ".docx"
    token = req_arr.get("dtoken")
    with open("pdp_schedule.txt", "a", encoding='utf-8') as f:
        json.dump({"file_name": file_name, "skills": skill_str, "pvq": pvq_dna, "prior_job_idx": prior_job_idx,"dtoken":token}, f,
                  ensure_ascii=False)
        f.write("\n")
    return file_name[-10:-5] + ".pdf"


@app.route("/pvg/report", methods=["POST"])
def pvg_report():
    req_arr = request.get_json(force=True)
    skill_str = req_arr["skill"]
    nid = req_arr["nid"]
    user = req_arr.get("user", "")
    file_name = random_str_generator() + ".docx"
    token = req_arr.get("dtoken")
    with open("pvg_schedule.txt", "a", encoding='utf-8') as f:
        json.dump({"file_name": file_name, "skills": skill_str, "nid": nid, "user": user,"dtoken":token}, f, ensure_ascii=False)
        f.write("\n")
    return file_name[-10:-5] + ".pdf"


@app.route("/storage/<file_name>", methods=["GET", "POST"])
def serve_storage_file(file_name):
    directory_path = os.path.join(app.root_path, "temp")
    if os.path.exists(os.path.join(directory_path, file_name)):
        try:
            return send_from_directory(directory_path, file_name)
        except Exception as ee:
            return str(ee)
    else:
        return {"status": "In progress"}


if __name__ == '__main__':
    app.run(debug=True, port=850)
