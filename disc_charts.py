import string
import random
from docx2pdf import convert

from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
import requests
from docx import Document
from docx.shared import Inches
from docx.text.paragraph import Paragraph
import os

script = os.path.realpath(__file__)
path = os.path.join(os.path.dirname(script),"temp")



def ready_documents(full_file_name, **kwargs):
    document = Document(kwargs.get("file_path"))
    document = replace_olgo_text(kwargs.get("olgo_text"), document)
    document = replace_gauge_charts(document, **kwargs)
    document = replace_line_chart(document, **kwargs)
    file_name = random_str_generator() + ".docx"
    document.save(full_file_name)


def align_paragraph_center(par: Paragraph):
    par.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER


def random_str_generator(character_num=5) -> str:
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=character_num))


def replace_olgo_text(text: str, document: Document):
    first_paragraph = document.tables[4].cell(0, 0).paragraphs[0]
    runs = first_paragraph.runs
    for index, run in enumerate(runs):
        if index == 0:
            run.text = text
        else:
            run.clear()
    return document


def replace_line_chart(document: Document, **kwargs):
    min_series = [{
        "name": '',
        "data": [["D", kwargs.get("min_d")], ["I", kwargs.get("min_i")], ["S", kwargs.get("min_s")],
                 ["C", kwargs.get("min_c")]],
        "color": "#A33333"
    }]
    min_pic = line_chart(min_series)
    min_table = document.tables[6]  # kamtarin table
    align_paragraph_center(min_table.cell(0, 1).paragraphs[0])
    min_table.cell(2, 0)._element.clear_content()
    min_table.cell(2, 0).add_paragraph().add_run().add_picture(min_pic, width=Inches(5.24))

    max_series = [{
        "name": '',
        "data": [["D", kwargs.get("max_d")], ["I", kwargs.get("max_i")], ["S", kwargs.get("max_s")],
                 ["C", kwargs.get("max_c")]],
        "color": "#3333A3"
    }]
    max_table = document.tables[7]  # bishtarin table
    max_pic = line_chart(max_series)
    align_paragraph_center(max_table.cell(0, 1).paragraphs[0])
    max_table.cell(2, 0)._element.clear_content()
    max_table.cell(2, 0).add_paragraph().add_run().add_picture(max_pic, width=Inches(5.24))

    min_series.extend(max_series)
    compare_series = min_series
    compare_table = document.tables[8]  # tatabogh table
    compare_pic = line_chart(compare_series)
    align_paragraph_center(compare_table.cell(0, 1).paragraphs[0])
    compare_table.cell(2, 0)._element.clear_content()
    compare_table.cell(2, 0).add_paragraph().add_run().add_picture(compare_pic, width=Inches(5.24))

    personality_series = [{
        "name": '',
        "data": [["D", kwargs.get("d_value")], ["I", kwargs.get("i_value")], ["S", kwargs.get("s_value")],
                 ["C", kwargs.get("c_value")]],
        "color": "#33A333"
    }]

    personlity_table = document.tables[9]  # shakhsiat table
    personlity_pic = line_chart(personality_series)
    align_paragraph_center(personlity_table.cell(0, 1).paragraphs[0])
    personlity_table.cell(2, 0)._element.clear_content()
    personlity_table.cell(2, 0).add_paragraph().add_run().add_picture(personlity_pic, width=Inches(5.24))
    return document


def export_chart_file(chart_options):
    export_dict = {"infile": chart_options, "type": "png"}
    r = requests.post(url="http://91.228.186.176:8889/", json=export_dict)
    file_name = random_str_generator() + ".png"
    file_path = os.path.join(path, file_name)

    with open(file_path, "wb")as f:
        f.write(r.content)
    return file_path


def line_chart(series_list):
    """

    :param series_list:
    for example
    [{
                         "name": '',
                         "data": [["D", 68], ["I", 63], ["S", 54], ["C", 50]],
                         "color": "#A33333"
                     }]
    :return:
    """
    chart_options = {"chart": {"type": 'line', "plotBorderWidth": 0, "plotBackgroundColor": '#fff',
                               "plotBackgroundImage": None, "width": 900},
                     "title": {"text": ''}, "exporting": {"enabled": False},
                     "credits": {"enabled": False},
                     "dataLabels": {"enabled": False},
                     "subtitle": {"text": ''},
                     "yAxis": {
                         "title": {"text": ''}, "max": 100, "min": 0, "tickInterval": 10,
                     }, "xAxis": {"type": 'category', },
                     "legend": {"enabled": False},
                     "plotOptions": {"series": {"data": ["D", "I", "S", "C"],
                                                }, "line": {"dataLabels": {"enabled": True}}
                                     },

                     "series": series_list,

                     }

    return export_chart_file(chart_options)


def replace_gauge_charts(document: Document, **kwargs):
    """

    :param document:
    :param kwargs:
    kwargs must include:  s_value, c_value, d_seg, i_seg, s_seg, c_seg
    :return:
    """

    table = document.tables[5]
    i_pic_cell = table.cell(0, 0)
    i_pic_cell._element.clear_content()  # remove old pic
    i_image = gauge_chart(tick_color="#FFA402", dial_color="#FFAE1A", value=kwargs.get("i_value"), chart_name="I")
    i_pic_cell.add_paragraph().add_run().add_picture(i_image, width=Inches(3.93))
    i_text_cell = table.cell(1, 0)
    gauge_table_text(i_text_cell, kwargs.get("i_seg"), kwargs.get("i_value"), "I")

    d_pic_cell = table.cell(0, 1)
    d_pic_cell._element.clear_content()  # remove old pic
    d_image = gauge_chart(tick_color="#FF2512", dial_color="red", value=kwargs.get("d_value"), chart_name="D")
    d_pic_cell.add_paragraph().add_run().add_picture(d_image, width=Inches(3.93))
    d_text_cell = table.cell(1, 1)
    gauge_table_text(d_text_cell, kwargs.get("d_seg"), kwargs.get("d_value"), "D")

    s_pic_cell = table.cell(2, 0)
    s_pic_cell._element.clear_content()  # remove old pic
    s_image = gauge_chart(tick_color="#007C00", dial_color="green", value=kwargs.get("s_value"), chart_name="S")
    s_pic_cell.add_paragraph().add_run().add_picture(s_image, width=Inches(3.93))
    s_text_cell = table.cell(3, 0)
    gauge_table_text(s_text_cell, kwargs.get("s_seg"), kwargs.get("s_value"), "S")

    c_pic_cell = table.cell(2, 1)
    c_pic_cell._element.clear_content()  # remove old pic
    c_image = gauge_chart(tick_color="#00007C", dial_color="blue", value=kwargs.get("c_value"), chart_name="C")
    c_pic_cell.add_paragraph().add_run().add_picture(c_image, width=Inches(3.93))
    c_text_cell = table.cell(3, 1)
    gauge_table_text(c_text_cell, kwargs.get("c_seg"), kwargs.get("c_value"), "C")
    return document


def en_num_to_farsi_num(input_text):
    input_text = str(input_text)
    intab = '12345678901234567890'
    outtab = '۱۲۳۴۵۶۷۸۹۰١٢٣٤٥٦٧٨٩٠'
    translation_table = str.maketrans(intab, outtab)
    return input_text.translate(translation_table)


def farsi_num_to_en_num(input_text):
    input_text = str(input_text)
    intab = '۱۲۳۴۵۶۷۸۹۰١٢٣٤٥٦٧٨٩٠'
    outtab = '12345678901234567890'
    translation_table = str.maketrans(intab, outtab)
    return input_text.translate(translation_table)


def gauge_chart(tick_color, dial_color, value, chart_name):
    chart_options = {
        "chart": {"type": 'gauge', "plotBorderWidth": 0, "plotBackgroundColor": '#fff',
                  "plotBackgroundImage": None, "height": 180, "width": 300, "marginBottom": -110
                  },
        "credits": {"enabled": False}, "title": {"text": None},
        "pane": [{"startAngle": -90, "endAngle": 90, "background": None, "center": ['50%'], "size": 250}],
        "exporting": {"enabled": False},
        "tooltip": {"enabled": False},
        "yAxis": [{"min": 0, "max": 100, "minorTickPosition": 'inside', "minTickInterval": 20, "tickInterval": 5,
                   "minorTickColor": tick_color, "tickPosition": 'outside',
                   "labels": {"rotation": 'auto', "distance": 12, "step": 2}, "pane": 0,
                   "title": {"padding": 1,
                             "text": f'<br/><span style="font-size:42px;color:#cecece">{chart_name}</span>', "y": 20}}
                  ],
        "plotOptions": {
            "gauge": {"dataLabels": {"enabled": False}, "dial": {"radius": '100%', "backgroundColor": dial_color}}
        },
        "series": [{"name": 'Channel A', "data": [value], "yAxis": 0}]
    }
    return export_chart_file(chart_options)


def gauge_table_text(text_cell, m_seg, m_value, m_name):
    text_cell.paragraphs[0].text = ""
    seg_statement = "سگمنت ۲ در بعد "
    text_cell.paragraphs[0].text = m_name + seg_statement.replace("۲", en_num_to_farsi_num(m_seg))
    align_paragraph_center(text_cell.paragraphs[0])
    text_cell.paragraphs[1].text = ""
    point_statement = "٪" + "امتیاز ۲۹ ".replace("۲۹", en_num_to_farsi_num(m_value))
    text_cell.paragraphs[1].text = point_statement
    align_paragraph_center(text_cell.paragraphs[1])


if __name__ == '__main__':
    mydict = {
        "file_path": "disc_word_template\CS.docx", "olgo_text": "الگوی رفتاری شما S با کد ۵-۴-۳-۱  می‌باشد.",
        "min_d": 44, "min_i": 22, "min_s": 11, "min_c": 17, "max_d": 74, "max_i": 22, "max_s": 34, "max_c": 44
        , "d_value": 55, "i_value": 66, "s_value": 95, "c_value": 44, "d_seg": 7, "i_seg": 5, "s_seg": 3, "c_seg": 2
    }

    full_name = ready_documents(**mydict)
    convert(full_name)
