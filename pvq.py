import os
import sys
from copy import deepcopy
from docx.enum.text import WD_BREAK
from PIL import Image
from docx.oxml import parse_xml
from docx.oxml.ns import nsdecls
from persiantools.jdatetime import JalaliDateTime
from disc_charts import en_num_to_farsi_num
from disc_charts import random_str_generator
import string
import random
from docx2pdf import convert

from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
import requests
from docx import Document
from docx.shared import Inches
from docx.text.paragraph import Paragraph


def pvq_gen_report(pvg_dna,full__file_name, nid, user=""):
    pvq_skill = ["خودرهبری", "هیجان‌خواهی", "لذت‌جویی", "موفقیت", "قدرت", "امنیت", "هم‌نوایی", "سنت‌گرایی", "خیرخواهی",
                 "جهان‌نگری "]
    script = os.path.realpath(__file__)
    path = os.path.dirname(script)
    sys.path.extend([path])
    doc1 = Document(os.path.join(path, "pvg_template.docx"))
    doc1.paragraphs[13].runs[1].text = nid
    doc1.paragraphs[14].runs[1].text = JalaliDateTime.now().strftime("%Y-%m-%d %H:%M-%S")
    doc1.paragraphs[15].runs[0].text = user
    doc1.tables[0].cell(0, 0).paragraphs[0].runs[0].text = pvq_skill[int(pvg_dna[0])]
    doc1.tables[0].cell(0, 1).paragraphs[0].runs[0].text = pvq_skill[int(pvg_dna[1])]
    doc1.tables[0].cell(0, 2).paragraphs[0].runs[0].text = pvq_skill[int(pvg_dna[2])]
    doc1.tables[1].cell(0, 0).paragraphs[0].runs[0].text = pvq_skill[int(pvg_dna[3])]
    doc1.tables[1].cell(0, 1).paragraphs[0].runs[0].text = pvq_skill[int(pvg_dna[4])]
    doc1.tables[1].cell(0, 2).paragraphs[0].runs[0].text = pvq_skill[int(pvg_dna[5])]
    doc1.tables[2].cell(0, 0).paragraphs[0].runs[0].text = pvq_skill[int(pvg_dna[6])]
    doc1.tables[2].cell(0, 1).paragraphs[0].runs[0].text = pvq_skill[int(pvg_dna[7])]
    doc1.tables[2].cell(0, 2).paragraphs[0].runs[0].text = pvq_skill[int(pvg_dna[8])]
    doc1.tables[3].cell(0, 0).paragraphs[0].runs[0].text = pvq_skill[int(pvg_dna[9])]
    doc1.save(full__file_name)
