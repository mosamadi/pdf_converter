from orator import DatabaseManager, Model


class DBConf:
    class __DBConf:
        def __init__(self):
            config = {
                'mysql': {
                    'driver': 'mysql',
                    'host': 'localhost',
                    'database': 'social_crawler',
                    'user': 'root',
                    'password': '',
                    'port': 3306,
                    'prefix': ''
                }
            }
            self.db = DatabaseManager(config)
            Model.set_connection_resolver(self.db)


        def __str__(self):
            return repr(self)

    instance = None

    def __init__(self):
        if not DBConf.instance:
            DBConf.instance = DBConf.__DBConf()

    def __getattr__(self, name):
        return getattr(self.instance, name)
