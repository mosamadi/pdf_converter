from docx import Document

from docx.oxml import OxmlElement
from docx.oxml.ns import qn

document = Document(r"C:\Users\Samadi\Desktop\report-clifton.docx")
table_cell = document.tables[0].cell(0, 0)
table_cell_props = table_cell._tc.get_or_add_tcPr()
clShading = OxmlElement('w:shd')
clShading.set(qn('w:fill'), "00519E")
table_cell_props.append(clShading)
shdx = document.tables[0].cell(0, 0)._tc.get_or_add_tcPr()
shd = OxmlElement("w:shd")


def colered_whole_dna(colered_patern, document):
    # 1: yellow 2:red 3:blue 4: purple
    color_dict = {"1": "F7B32E", "2": "9C1C20", "3": "204B75", "4": "5D3A6D"}
    left_table = document.tables[0].cell(0, 2).tables[0]
    right_table = document.tables[0].cell(0, 0).tables[0]

    for i in range(0, 10):
        colored_cell(left_table.cell(0, (2 * i)), color_dict[colered_patern[i]])
        colored_cell(left_table.cell(0, (2 * i) + 1), "FFFFFF")

    for i in range(0, 24):
        colored_cell(right_table.cell(0, (2 * i)), color_dict[colered_patern[i + 10]])
        colored_cell(right_table.cell(0, (2 * i) + 1), "FFFFFF")


def colored_cell(cell, color):
    table_cell_props = cell._tc.get_or_add_tcPr()
    clShading = OxmlElement('w:shd')
    clShading.set(qn('w:fill'), color)
    table_cell_props.append(clShading)
