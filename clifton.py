import os
import sys
from copy import deepcopy
from docx.enum.text import WD_BREAK
from PIL import Image
from docx.oxml import parse_xml
from docx.oxml.ns import nsdecls
from persiantools.jdatetime import JalaliDateTime
from disc_charts import en_num_to_farsi_num
from disc_charts import random_str_generator
import string
import random
from docx2pdf import convert

from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
import requests
from docx import Document
from docx.shared import Inches
from docx.text.paragraph import Paragraph

SkillsInfo = [{'id': 1, 'color': '1', 'name': ' کامجو', 'tbl': 7},
              {'id': 2, 'color': '2', 'name': ' عملگرا', 'tbl': 12},
              {'id': 3, 'color': '3', 'name': ' سازگار', 'tbl': 32},
              {'id': 4, 'color': '4', 'name': ' تحلیلگر', 'tbl': 10},
              {'id': 5, 'color': '1', 'name': ' گرداننده', 'tbl': 16},
              {'id': 6, 'color': '1', 'name': '  ارزشمدار', 'tbl': 9},
              {'id': 7, 'color': '2', 'name': ' فرمانده', 'tbl': 26},
              {'id': 8, 'color': '2', 'name': ' سخنور', 'tbl': 15},
              {'id': 9, 'color': '2', 'name': ' رقابت جو', 'tbl': 11},
              {'id': 10, 'color': '3', 'name': ' پیوسته نگر', 'tbl': 25},
              {'id': 11, 'color': '1', 'name': ' عدالت طلب', 'tbl': 27},
              {'id': 12, 'color': '4', 'name': ' پیشینه/زمینه گرا', 'tbl': 29},
              {'id': 13, 'color': '1', 'name': ' محتاط', 'tbl': 22},
              {'id': 14, 'color': '3', 'name': ' پرورش دهنده', 'tbl': 23},
              {'id': 15, 'color': '1', 'name': ' منظم', 'tbl': 14},
              {'id': 16, 'color': '3', 'name': ' همدل', 'tbl': 28},
              {'id': 17, 'color': '1', 'name': ' متمرکز بر هدف', 'tbl': 3},
              {'id': 18, 'color': '4', 'name': ' آینده نگر', 'tbl': 1},
              {'id': 19, 'color': '3', 'name': ' تعارض گریز', 'tbl': 30},
              {'id': 20, 'color': '4', 'name': ' ایده پرداز', 'tbl': 19},
              {'id': 21, 'color': '3', 'name': ' گردآورنده افراد', 'tbl': 33},
              {'id': 22, 'color': '3', 'name': ' فردنگر', 'tbl': 2},
              {'id': 23, 'color': '4', 'name': ' کلکسیونر', 'tbl': 21},
              {'id': 24, 'color': '4', 'name': ' متفکر', 'tbl': 17},
              {'id': 25, 'color': '4', 'name': ' یادگیرنده', 'tbl': 6},
              {'id': 26, 'color': '2', 'name': '  ایده آل گرا', 'tbl': 4},
              {'id': 27, 'color': '3', 'name': ' مثبتگرا', 'tbl': 18},
              {'id': 28, 'color': '3', 'name': ' صمیمت طلب', 'tbl': 24},
              {'id': 29, 'color': '1', 'name': ' مسئولیت‌پذیر', 'tbl': 8},
              {'id': 30, 'color': '1', 'name': ' حلال مسائل', 'tbl': 31},
              {'id': 31, 'color': '2', 'name': ' متکی به خود', 'tbl': 13},
              {'id': 32, 'color': '2', 'name': 'اهمیت طلب', 'tbl': 0},
              {'id': 33, 'color': '4', 'name': ' استراتژیک', 'tbl': 5},
              {'id': 34, 'color': '2', 'name': ' اجتماعی', 'tbl': 20}]


def char_decode(ch):
    ch_index = ord(ch)
    if ch_index <= ord("Z"):
        return ch_index - ord("A")
    else:
        return ch_index - ord("a") + 26


def find_dna_color(skill_str: str):
    """
    :param skill_str: skill string based on ascii code
    :return: clifton color dna
    """
    result = ""
    for ch in skill_str:
        c_index = char_decode(ch)
        result += SkillsInfo[c_index]["color"]
    return result


def edit_all_skill_tbl(document: Document, skill_str):
    hex_color_list = ["5d3a6d", "f7b32e", "204b75", "9c1c20"]
    for loop_index, ch in enumerate(skill_str):
        c_index = char_decode(ch)
        hex_color = hex_color_list[int(SkillsInfo[c_index]["color"]) - 1]
        shading_elm_1 = parse_xml((r'<w:shd {} w:fill="' + hex_color + r'"/>').format(nsdecls('w')))
        if loop_index < 10:
            document.tables[1].cell(2 * loop_index + 1, 8).paragraphs[0].runs[0].text = SkillsInfo[c_index]["name"]
            document.tables[1].cell(2 * loop_index + 1, 6)._tc.get_or_add_tcPr().append(shading_elm_1)
            shading_elm_1 = parse_xml((r'<w:shd {} w:fill="' + hex_color + r'"/>').format(nsdecls('w')))
            document.tables[3].cell(2 * loop_index + 1, 2).paragraphs[0].runs[0].text = SkillsInfo[c_index]["name"]
            document.tables[3].cell(2 * loop_index + 1, 0)._tc.get_or_add_tcPr().append(shading_elm_1)
        elif loop_index < 22:
            document.tables[1].cell(2 * loop_index - 19, 5).paragraphs[0].runs[0].text = SkillsInfo[c_index]["name"]
            document.tables[1].cell(2 * loop_index - 19, 3)._tc.get_or_add_tcPr().append(shading_elm_1)
        else:
            document.tables[1].cell(2 * loop_index - 43, 2).paragraphs[0].runs[0].text = SkillsInfo[c_index]["name"]
            document.tables[1].cell(2 * loop_index - 43, 0)._tc.get_or_add_tcPr().append(shading_elm_1)
    return document


def add_skill_tbls(document: Document, all_skill: Document, skill_str: str):
    """
    :param document: document to edit
    :param skill_str: skill string based on ascii code (all code plus 65 because started with meaning char in GET req)
    :param all_skill: document which contain all skills definition
    :return: changed document
    """
    skill_str = skill_str[0:10]
    skill_str = skill_str[::-1]

    for iloop, ch in enumerate(skill_str):
        c_index = char_decode(ch)
        tbl_index = SkillsInfo[c_index]["tbl"]
        color_index = SkillsInfo[c_index]["color"]
        document.paragraphs[40]._p.addnext(deepcopy(all_skill.tables[tbl_index]._tbl))
        document.tables[4].cell(0, 0)._element.clear()
        document.tables[4].cell(0, 0).add_paragraph()
        skill_png = fill_color_clifton_dna(((9 - iloop) * "0") + color_index + (iloop * "0") + "0" * 24)
        document.tables[4].cell(0, 0).paragraphs[0].add_run().add_picture(skill_png, width=Inches(6.12),
                                                                          height=Inches(0.6))
        if iloop != 9:
            mypar = document.add_paragraph()
            mypar.add_run().add_break(WD_BREAK.PAGE)
            document.paragraphs[40]._p.addnext(mypar._p)

    return document


def fill_color_clifton_dna(str_dna):
    """
    fill by color based on strdna the dna of skills  colors: 1:purple 2:yellow 3:blue 4: red 0: gray
    :rtype: str
    :param str_dna 34 skills str
    :type str_dna:str
    :return: file_name
    """

    colors_list = [(191, 191, 191, 191), (93, 58, 109), (247, 179, 46), (32, 75, 117), (156, 28, 32)]
    script = os.path.realpath(__file__)
    path = os.path.dirname(script)
    file_name = os.path.join(path, "temp", random_str_generator() + ".png")
    picture = Image.open(os.path.join(path, "clifton_dna.png"))
    x_ranges = [(44, 52), (61, 69), (78, 86), (95, 103), (111, 120), (128, 137), (145, 153), (162, 170), (179, 187),
                (196, 204), (297, 305), (314, 322), (330, 339), (347, 356), (364, 372), (381, 389), (398, 406),
                (415, 423), (431, 440), (448, 457), (465, 473), (482, 490), (499, 507), (516, 524), (532, 541),
                (549, 558), (566, 574), (583, 591), (600, 608), (617, 625), (633, 642), (650, 659), (667, 675),
                (684, 692)]

    y_range = (13, 54)

    for index, ch in enumerate(str_dna):
        if ch != "0" and ch.isdigit() and int(ch) < 5:
            for x in range(*x_ranges[index]):
                for y in range(*y_range):
                    picture.putpixel((x, y), colors_list[int(ch)])
    picture.save(file_name)
    return file_name


def clifton_rep_gen(skill_str, full__file_name, nid, user=""):
    script = os.path.realpath(__file__)
    path = os.path.dirname(script)
    sys.path.extend([path])
    sample_dna = find_dna_color(skill_str)
    doc1 = Document(os.path.join(path, "report-clifton2.docx"))
    all_skill = Document(os.path.join(path, "all_skills.docx"))
    # adding skills tables
    add_skill_tbls(doc1, all_skill, skill_str)
    edit_all_skill_tbl(doc1, skill_str)
    ####

    doc1.paragraphs[14].runs[1].text = nid
    doc1.paragraphs[15].runs[2].text = JalaliDateTime.now().strftime("%Y-%m-%d %H:%M-%S")
    doc1.paragraphs[16].runs[0].text = user
    all_skill_png = fill_color_clifton_dna(sample_dna)
    top_skill_png = fill_color_clifton_dna(sample_dna[0:10] + "0" * 10)
    doc1.tables[0].cell(0, 0).paragraphs[0].clear()
    doc1.tables[0].cell(0, 0).paragraphs[0].add_run().add_picture(all_skill_png, width=Inches(6.12), height=Inches(0.6))
    doc1.tables[2].cell(0, 0).paragraphs[0].clear()
    doc1.tables[2].cell(0, 0).paragraphs[0].add_run().add_picture(top_skill_png, width=Inches(6.12), height=Inches(0.6))

    doc1.save(full__file_name)


if __name__ == '__main__':
    clifton = {"file_name": "TRPFB.docx", "skills": "HOYTBXbEVKLZGIfaCNSUeDgQMJhdRcFWPA", "nid": "0850124026",
               "user": "محمد صمدی"}
    clifton_rep_gen(clifton["skills"], "./temp/" + clifton["file_name"], clifton["nid"], user=clifton["user"])

